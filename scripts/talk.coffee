module.exports = (robot) ->
  robot.respond /(\S+)/i, (msg) ->
    DOCOMO_API_KEY = process.env.DOCOMO_API_KEY
    message = msg.match[1]
    return unless DOCOMO_API_KEY && message

    KEY_DOCOMO_CONTEXT = 'docomo-talk-context'
    context = robot.brain.get KEY_DOCOMO_CONTEXT || ''

    url = 'https://api.apigw.smt.docomo.ne.jp/dialogue/v1/dialogue?APIKEY=' + DOCOMO_API_KEY
    user_name = msg.message.user.name
  
    request = require('request');
    request.post
      url: url
      json:
        utt: message
        context: context if context
        t: 20
        nickname: user_name if user_name
      , (err, response, body) ->
  
        robot.brain.set KEY_DOCOMO_CONTEXT, body.context

        msg.send body.utt
